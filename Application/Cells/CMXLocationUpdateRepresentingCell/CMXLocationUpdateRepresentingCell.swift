//
//  CMXLocationUpdateRepresentingCell.swift
//  VSensor
//
//  Created by Virinchi Softwares on 14/09/16.
//  Copyright © 2016 Virinchi Softwares. All rights reserved.
//

import UIKit
import CoreMotion

/// <#Description#>
class CMXLocationUpdateRepresentingCell: UITableViewCell {
    
    @IBOutlet weak var infoLabel1: UILabel!
    @IBOutlet weak var infoLabel2: UILabel!
    @IBOutlet weak var infoLabel3: UILabel!
    @IBOutlet weak var infoLabel4: UILabel!
    
}
