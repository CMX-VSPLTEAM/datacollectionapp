//
//  VSMotionDisplayViewControllerType2.swift
//  VSensor
//
//  Created by Virinchi Softwares on 14/09/16.
//  Copyright © 2016 Virinchi Softwares. All rights reserved.
//


import UIKit
import JBChartView
import CoreMotion

/// <#Description#>
class VSMotionDisplayViewControllerType2: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    /// <#Description#>
    @IBOutlet weak var stepsLabel: UILabel!
    @IBOutlet weak var floorsLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var altitudeLabel: UILabel!
    @IBOutlet weak var motionLabel: UILabel!
    @IBOutlet weak var activitiesTableView: UITableView!
    
    var altitudeChange: Double = 0
    var activityCollection: VSActivityCollection?
    
    
    /// <#Description#>
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        startupInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    
    /// <#Description#>
    func setupForNavigationBar(){
    }
    
    /// <#Description#>
    func registerForNotifications(){
    }
    
    /// <#Description#>
    func startupInitialisations(){
        VSMotionController.sharedInstance.startUpdatingActivities({[weak self] (altitude) in
            guard let `self` = self else { return }
            self.altitudeChange = altitude.relativeAltitude as Double
            self.altitudeLabel.text = String(format: "%.3f",self.altitudeChange)
            if VSDataManager.sharedInstance.altitudeLatestDataToDisplay.count < KMaxObservationCount {
                VSDataManager.sharedInstance.altitudeLatestDataToDisplay.add(VSAltitude(altitide: altitude, capturedAt: NSDate() as Date))
            }else{
                VSDataManager.sharedInstance.altitudeLatestDataToDisplay.removeObject(at: 0)
                VSDataManager.sharedInstance.altitudeLatestDataToDisplay.add(VSAltitude(altitide: altitude, capturedAt: NSDate() as Date))
            }
            }, { (pedometer) in
                self.floorsLabel.text = "\(safeInt(object: pedometer.floorsAscended, alternate: 0))"
                self.stepsLabel.text = "\(safeInt(object: pedometer.numberOfSteps, alternate: 0))"
                self.distanceLabel.text = String(format: "%.3fm",safeDouble(object: pedometer.distance, alternate: 0))
                if VSDataManager.sharedInstance.pedoMeterLatestDataToDisplay.count < KMaxObservationCount {
                    VSDataManager.sharedInstance.pedoMeterLatestDataToDisplay.add(VSPedometer(pedometer: pedometer, capturedAt: NSDate() as Date))
                }else{
                    VSDataManager.sharedInstance.pedoMeterLatestDataToDisplay.removeObject(at: 0)
                    VSDataManager.sharedInstance.pedoMeterLatestDataToDisplay.add(VSPedometer(pedometer: pedometer, capturedAt: NSDate() as Date))
                }
        }) { (motionActivity) in
            if motionActivity.running {
                self.motionLabel.text = "Running"
            } else if motionActivity.cycling {
                self.motionLabel.text = "Cycling"
            } else if motionActivity.walking {
                self.motionLabel.text = "Walking"
            }else if motionActivity.automotive {
                self.motionLabel.text = "Driving"
            }else if motionActivity.unknown {
                self.motionLabel.text = "Unknown"
            }else if motionActivity.stationary {
                self.motionLabel.text = "Stationary"
            }
            if VSDataManager.sharedInstance.activityLatestDataToDisplay.count < KMaxObservationCount {
                VSDataManager.sharedInstance.activityLatestDataToDisplay.add(VSMotionActivity(motionActivity: motionActivity, capturedAt: NSDate() as Date))
            }else{
                VSDataManager.sharedInstance.activityLatestDataToDisplay.removeObject(at: 0)
                VSDataManager.sharedInstance.activityLatestDataToDisplay.add(VSMotionActivity(motionActivity: motionActivity, capturedAt: NSDate() as Date))
            }
        }
        self.activitiesTableView.delegate = self
        self.activitiesTableView.dataSource = self
        VSMotionController.sharedInstance.fetchHistoricalMotionActivities { (activities) in
            self.activityCollection = VSActivityCollection(activities: activities!)
            self.activitiesTableView.reloadData()
        }
    }
    
    /// <#Description#>
    func updateUserInterfaceOnScreen(){
        
    }
    
    /// <#Description#>
    ///
    /// - parameter tableView: <#tableView description#>
    /// - parameter section:   <#section description#>
    ///
    /// - returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activityCollection?.activities.count ?? 0
    }
    
    
    /// <#Description#>
    ///
    /// - parameter date: <#date description#>
    ///
    /// - returns: <#return value description#>
    func dateStringForDate(date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        return dateFormatter.string(from: date)
    }
    
    /// <#Description#>
    ///
    /// - parameter tableView: <#tableView description#>
    /// - parameter indexPath: <#indexPath description#>
    ///
    /// - returns: <#return value description#>
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let activityRepresentingCell = tableView.dequeueReusableCell(withIdentifier: "ActivityRepresentingCell", for: indexPath as IndexPath) as! ActivityRepresentingCell
        let activity = self.activityCollection?.activities[indexPath.row]
        VSMotionController.sharedInstance.fetchPedometerData(from: (activity?.startDate)!, to: (activity?.endDate)!) { (pedometerData) in
            
            activityRepresentingCell.infoLabel4.text = "\(self.dateStringForDate(date: activity!.startDate as Date)) to \(self.dateStringForDate(date: activity!.endDate as Date))"
            
            if CMPedometer.isStepCountingAvailable() {
                activityRepresentingCell.infoLabel1.text = "Steps : \(safeInt(object: pedometerData.numberOfSteps, alternate: 0))"
            }else{
                activityRepresentingCell.infoLabel1.text = "Steps : --- "
            }
            if CMPedometer.isDistanceAvailable() {
                activityRepresentingCell.infoLabel2.text = "Dis : \(String(format: "%.3f",safeDouble(object: pedometerData.distance, alternate: 0.0)))"
            }else{
                activityRepresentingCell.infoLabel2.text = "Dis : --- "
            }
            if CMPedometer.isFloorCountingAvailable() {
                activityRepresentingCell.infoLabel3.text = "Floor : \(safeInt(object: pedometerData.floorsAscended, alternate: 0))"
            }else{
                activityRepresentingCell.infoLabel3.text = "Floor : --- "
            }
        }
        return activityRepresentingCell
    }
    
    /// <#Description#>
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

