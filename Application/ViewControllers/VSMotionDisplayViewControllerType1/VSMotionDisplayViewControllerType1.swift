//
//  VSMotionDisplayViewControllerType1.swift
//  VSensor
//
//  Created by Virinchi Softwares on 14/09/16.
//  Copyright © 2016 Virinchi Softwares. All rights reserved.
//


import UIKit
import JBChartView
import CoreMotion

/// <#Description#>
class VSMotionDisplayViewControllerType1: UIViewController ,JBBarChartViewDelegate ,JBBarChartViewDataSource  , UINavigationControllerDelegate , UITextFieldDelegate{
    
    @IBOutlet var accelerometerXLabel : UILabel!
    @IBOutlet var accelerometerYLabel : UILabel!
    @IBOutlet var accelerometerZLabel : UILabel!
    @IBOutlet var accelerometerGraphContainerView : UIView!
    var barChartViewAccelerometer : JBBarChartView!
    
    @IBOutlet var gyroscopeXLabel : UILabel!
    @IBOutlet var gyroscopeYLabel : UILabel!
    @IBOutlet var gyroscopeZLabel : UILabel!
    @IBOutlet var gyroscopeGraphContainerView : UIView!
    var barChartViewGyroscope : JBBarChartView!

    @IBOutlet var magnetometerXLabel : UILabel!
    @IBOutlet var magnetometerYLabel : UILabel!
    @IBOutlet var magnetometerZLabel : UILabel!
    @IBOutlet var magnetometerGraphContainerView : UIView!
    var barChartViewMagnetometer : JBBarChartView!

    @IBOutlet var yawLabel : UILabel!
    @IBOutlet var pitchLabel : UILabel!
    @IBOutlet var rollLabel : UILabel!

    @IBOutlet var recordingSwitch : UISwitch!
    @IBOutlet var recordingStatusLabel : UILabel!

    @IBOutlet var motionUpdateIntervalSlider : UISlider!
    @IBOutlet var motionUpdateIntervalInfoLabel : UILabel!
    
    @IBOutlet var observerNameTextField : UITextField!
    @IBOutlet var observerActionSegmentControl : UISegmentedControl!

    /// <#Description#>
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        startupInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    /// <#Description#>
    func setupForNavigationBar(){
    }
    
    /// <#Description#>
    func registerForNotifications(){
    }
    
    /// <#Description#>
    func setupMotionUpdates(){
        JBBarChartView.setAnimationInterval(CGFloat(MOTION_UPDATE_INTERVAL))
        VSMotionController.sharedInstance.startMotionUpdates({ [weak self](magneticField) in
            guard let `self` = self else { return }
            self.updateMagnetometerDataOnScreen(magneticField: magneticField)
            }, { [weak self](acceleration) in
                guard let `self` = self else { return }
                self.updateAccelerometerDataOnScreen(accelerate: acceleration)
            }, { [weak self](rotation) in
                guard let `self` = self else { return }
                self.updateGyroscopeDataOnScreen(rotation: rotation)
        }) { [weak self](attitude) in
            guard let `self` = self else { return }
            self.updateAttitude(attitude: attitude)
        }
    }
    
    /// start Motion manager with default settings
    func startupInitialisations(){
        recordingSwitch.isOn = false
        updateRecordingInformationLabel()
        if let cachedText = UserDefaults.standard.object(forKey: "observerNameTextField") as? String{
            observerNameTextField.text = cachedText
        }
        VSDataManager.sharedInstance.observerName = observerNameTextField.text ?? "Guest"
        observerNameTextField.returnKeyType = .go
        motionUpdateIntervalSlider.value = Float(MOTION_UPDATE_INTERVAL)
        motionUpdateIntervalSlider.minimumValue = Float(MOTION_UPDATE_INTERVAL_MINIMUM_VALUE)
        motionUpdateIntervalSlider.maximumValue = Float(MOTION_UPDATE_INTERVAL)*10
        updateMotionUpdateIntervalInfoLabel()
        func setupAccelerometer() {
            barChartViewAccelerometer = JBBarChartView(frame: accelerometerGraphContainerView.bounds)
            barChartViewAccelerometer.dataSource = self
            barChartViewAccelerometer.delegate = self
            accelerometerGraphContainerView.addSubview(barChartViewAccelerometer)
            barChartViewAccelerometer.reloadData()
        }
        func setupGyroScope() {
            barChartViewGyroscope = JBBarChartView(frame: gyroscopeGraphContainerView.bounds)
            barChartViewGyroscope.dataSource = self
            barChartViewGyroscope.delegate = self
            gyroscopeGraphContainerView.addSubview(barChartViewGyroscope)
            barChartViewGyroscope.reloadData()
        }
        func setupMagnetometer() {
            barChartViewMagnetometer = JBBarChartView(frame: accelerometerGraphContainerView.bounds)
            barChartViewMagnetometer.dataSource = self
            barChartViewMagnetometer.delegate = self
            magnetometerGraphContainerView.addSubview(barChartViewMagnetometer)
            barChartViewMagnetometer.reloadData()
        }
 
        setupAccelerometer()
        setupGyroScope()
        setupMagnetometer()
        setupMotionUpdates()
    }
    
    /// update accelerometer bars
    ///
    /// - parameter accelerate: Accelerometer readings
    func updateAccelerometerDataOnScreen(accelerate:VSAccelerate) {
        accelerometerXLabel.text = "x: \(String(format: "%.3f",accelerate.x))"
        accelerometerYLabel.text = "y: \(String(format: "%.3f",accelerate.y))"
        accelerometerZLabel.text = "z: \(String(format: "%.3f",accelerate.z))"
        if VSDataManager.sharedInstance.accelerometerLatestDataToDisplay.count < KMaxObservationCount {
            VSDataManager.sharedInstance.accelerometerLatestDataToDisplay.add(accelerate)
        }else{
            VSDataManager.sharedInstance.accelerometerLatestDataToDisplay.removeObject(at: 0)
            VSDataManager.sharedInstance.accelerometerLatestDataToDisplay.add(accelerate)
        }
        barChartViewAccelerometer.reloadData(animated: true)
    }
    
    /// update gyroscoper bars
    ///
    /// - parameter rotation: Gyroscope readings
    func updateGyroscopeDataOnScreen(rotation:VSRotation) {
        gyroscopeXLabel.text = "x: \(String(format: "%.3f",rotation.x))"
        gyroscopeYLabel.text = "y: \(String(format: "%.3f",rotation.y))"
        gyroscopeZLabel.text = "z: \(String(format: "%.3f",rotation.z))"

        if VSDataManager.sharedInstance.rotationLatestDataToDisplay.count < KMaxObservationCount {
            VSDataManager.sharedInstance.rotationLatestDataToDisplay.add(rotation)
        }else{
            VSDataManager.sharedInstance.rotationLatestDataToDisplay.removeObject(at: 0)
            VSDataManager.sharedInstance.rotationLatestDataToDisplay.add(rotation)
        }
        barChartViewGyroscope.reloadData(animated: true)
    }
    
    /// update magnetometer bars
    ///
    /// - parameter magneticField: magenetometer reading
    func updateMagnetometerDataOnScreen(magneticField:VSMagneticField) {
        magnetometerXLabel.text = "x: \(String(format: "%.3f",magneticField.x))"
        magnetometerYLabel.text = "y: \(String(format: "%.3f",magneticField.y))"
        magnetometerZLabel.text = "z: \(String(format: "%.3f",magneticField.z))"
        if VSDataManager.sharedInstance.isRecording {
            if VSDataManager.sharedInstance.magneticFieldLatestDataToDisplay.count < KMaxObservationCount {
                VSDataManager.sharedInstance.magneticFieldLatestDataToDisplay.add(magneticField)
            }else{
                VSDataManager.sharedInstance.magneticFieldLatestDataToDisplay.removeObject(at: 0)
                VSDataManager.sharedInstance.magneticFieldLatestDataToDisplay.add(magneticField)
            }
        }
        barChartViewMagnetometer.reloadData(animated: true)
    }
    
    /// update Attitude values
    ///
    /// - parameter attitude: Attitude readings
    func updateAttitude(attitude:VSAttitude){
        yawLabel.text = "Yaw : \(String(format: "%.1f",(180/M_PI)*attitude.yaw))°"
        pitchLabel.text = "Pitch : \(String(format: "%.1f",(180/M_PI)*attitude.pitch))°"
        rollLabel.text = "Roll : \(String(format: "%.1f",(180/M_PI)*attitude.roll))°"
        if VSDataManager.sharedInstance.attitudeLatestDataToDisplay.count < KMaxObservationCount {
            VSDataManager.sharedInstance.attitudeLatestDataToDisplay.add(attitude)
        }else{
            VSDataManager.sharedInstance.attitudeLatestDataToDisplay.removeObject(at: 0)
            VSDataManager.sharedInstance.attitudeLatestDataToDisplay.add(attitude)
        }
    }
    
    /// update motion frequency
    func updateMotionUpdateIntervalInfoLabel() {
        motionUpdateIntervalInfoLabel.text = "Motion Update interval : \((String(format: "%.3f",motionUpdateIntervalSlider.value))) Seconds"
    }
    
    func updateUserInterfaceOnScreen(){
        
    }
    
    /// return number of bars in a chart
    ///
    /// - parameter barChartView: current chart
    ///
    /// - returns: number of bars
    func numberOfBars(in barChartView: JBBarChartView!) -> UInt {
        if barChartView == barChartViewAccelerometer {
            if VSDataManager.sharedInstance.accelerometerLatestDataToDisplay.count > 0 {
                return 3
            }
        }else if barChartView == barChartViewGyroscope {
            if VSDataManager.sharedInstance.rotationLatestDataToDisplay.count > 0 {
                return 3
            }
        }else if barChartView == barChartViewMagnetometer {
            if VSDataManager.sharedInstance.magneticFieldLatestDataToDisplay.count > 0 {
                return 3
            }
        }
        return 0
    }
    
    /// calculate bar height
    ///
    /// - parameter barChartView: current chart which needs height
    /// - parameter index:        x, y or z
    ///
    /// - returns: height of bar
    func barChartView(_ barChartView: JBBarChartView!, heightForBarViewAt index: UInt) -> CGFloat {
        if barChartView == barChartViewAccelerometer {
            let info  = VSDataManager.sharedInstance.accelerometerLatestDataToDisplay.lastObject as! VSAccelerate
            if index == 0 {
                return CGFloat(abs(info.x))
            }else if index == 1 {
                return CGFloat(abs(info.y))
            }else if index == 2 {
                return CGFloat(abs(info.z))
            }
            return 0
        }else if barChartView == barChartViewGyroscope {
            let info  = VSDataManager.sharedInstance.rotationLatestDataToDisplay.lastObject as! VSRotation
            if index == 0 {
                return CGFloat(abs(info.x))
            }else if index == 1 {
                return CGFloat(abs(info.y))
            }else if index == 2 {
                return CGFloat(abs(info.z))
            }
            return 0
        }else if barChartView == barChartViewMagnetometer {
            let info  = VSDataManager.sharedInstance.magneticFieldLatestDataToDisplay.lastObject as! VSMagneticField
            if index == 0 {
                return CGFloat(abs(info.x))
            }else if index == 1 {
                return CGFloat(abs(info.y))
            }else if index == 2 {
                return CGFloat(abs(info.z))
            }
            return 0
        }
        return 0
    }
    
    /// get the bar chart color
    ///
    /// - parameter barChartView: current chart which need to get drawn
    /// - parameter index:        x,y or z
    ///
    /// - returns: Color
    public func barChartView(_ barChartView: JBBarChartView!, colorForBarViewAt index: UInt) -> UIColor! {
        if barChartView == barChartViewAccelerometer {
            let info  = VSDataManager.sharedInstance.accelerometerLatestDataToDisplay.lastObject as! VSAccelerate
            if index == 0 {
                return info.x < 0 ? KNegativeColorValue : KPositiveColorValue
            }else if index == 1 {
                return info.y < 0 ? KNegativeColorValue : KPositiveColorValue
            }else if index == 2 {
                return info.z < 0 ? KNegativeColorValue : KPositiveColorValue
            }
        }else if barChartView == barChartViewGyroscope {
            let info  = VSDataManager.sharedInstance.rotationLatestDataToDisplay.lastObject as! VSRotation
            if index == 0 {
                return info.x < 0 ? KNegativeColorValue : KPositiveColorValue
            }else if index == 1 {
                return info.y < 0 ? KNegativeColorValue : KPositiveColorValue
            }else if index == 2 {
                return info.z < 0 ? KNegativeColorValue : KPositiveColorValue
            }
        }else if barChartView == barChartViewMagnetometer {
            let info  = VSDataManager.sharedInstance.magneticFieldLatestDataToDisplay.lastObject as! VSMagneticField
            if index == 0 {
                return info.x < 0 ? KNegativeColorValue : KPositiveColorValue
            }else if index == 1 {
                return info.y < 0 ? KNegativeColorValue : KPositiveColorValue
            }else if index == 2 {
                return info.z < 0 ? KNegativeColorValue : KPositiveColorValue
            }
        }
        return KPositiveColorValue
    }
    
    /// <#Description#>
    ///
    /// - parameter degrees: <#degrees description#>
    ///
    /// - returns: <#return value description#>
    func radians(fromDegrees degrees: Double) -> Double {
        return 180 * degrees / M_PI
    }
    
    /// <#Description#>
    @IBAction func motionUpdateIntervalSliderChanged() {
        if VSMotionController.sharedInstance.isMotionManagerRunning() {
            MOTION_UPDATE_INTERVAL = Double(motionUpdateIntervalSlider.value)
            JBBarChartView.setAnimationInterval(CGFloat(MOTION_UPDATE_INTERVAL))
            VSMotionController.sharedInstance.updateMotionUpdateTimeInterval()
            updateMotionUpdateIntervalInfoLabel()
        }
    }
    
    
    /// AlertController action
    @IBAction func onClickOfOptions() {
        let actionSheetController: UIAlertController = UIAlertController(title: "Please select", message: "What you would like to perform?", preferredStyle: .actionSheet)
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
        }
        let shareAction: UIAlertAction = UIAlertAction(title: "Share", style: .default)
        { action -> Void in
            VSDataManager.sharedInstance.share()
        }
        actionSheetController.addAction(cancelAction)
        actionSheetController.addAction(shareAction)

        if UI_USER_INTERFACE_IDIOM() == .pad{
            actionSheetController.popoverPresentationController?.sourceView = self.view
        }else{
            self.present(actionSheetController, animated: true, completion: nil)
        }
    }
    
    /// <#Description#>
    ///
    /// - parameter textField: <#textField description#>
    ///
    /// - returns: <#return value description#>
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    /// <#Description#>
    ///
    /// - parameter textField: <#textField description#>
    func textFieldDidEndEditing(_ textField: UITextField) {
        VSDataManager.sharedInstance.observerName = textField.text ?? "Guest"
        if let text = textField.text as? NSString {
            UserDefaults.standard.set(text, forKey: "observerNameTextField")
        }
    }
    
    /// <#Description#>
    @IBAction func segmentControlValueChanged() {
        if observerActionSegmentControl.selectedSegmentIndex == 0 {
            VSDataManager.sharedInstance.observerAction = "Stationary"
        }else if observerActionSegmentControl.selectedSegmentIndex == 1 {
            VSDataManager.sharedInstance.observerAction = "Walking"
        }else if observerActionSegmentControl.selectedSegmentIndex == 2 {
            VSDataManager.sharedInstance.observerAction = "Random"
        }else if observerActionSegmentControl.selectedSegmentIndex == 3 {
            VSDataManager.sharedInstance.observerAction = "Step Up"
        }else if observerActionSegmentControl.selectedSegmentIndex == 4 {
            VSDataManager.sharedInstance.observerAction = "Step Down"
        }
    }
    
    func updateRecordingInformationLabel() {
        if !recordingSwitch.isOn {
            recordingStatusLabel.text = "Recording Paused"
        }else{
            recordingStatusLabel.text = "Recording Events"
        }
    }
    
    @IBAction func recordingSwitchValueChanged() {
        VSDataManager.sharedInstance.isRecording = recordingSwitch.isOn
        updateRecordingInformationLabel()
    }
    
    
    /// <#Description#>
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

