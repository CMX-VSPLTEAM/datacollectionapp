//
//  VSMotionDisplayViewControllerType3.swift
//  VSensor
//
//  Created by Virinchi Softwares on 14/09/16.
//  Copyright © 2016 Virinchi Softwares. All rights reserved.
//


import UIKit
import JBChartView
import CoreMotion

/// <#Description#>
class VSMotionDisplayViewControllerType3: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var cmxLocationXCoordinateLabel: UILabel!
    @IBOutlet weak var cmxLocationYCoordinateLabel: UILabel!
    @IBOutlet weak var cmxLocationZCoordinateLabel: UILabel!
    @IBOutlet weak var cmxLocationServerTimeStamp: UILabel!
    @IBOutlet weak var cmxLocationLocalTimeStamp: UILabel!
    @IBOutlet weak var cmxLocationTableView: UITableView!
    @IBOutlet var baseUrlTextField : UITextField!
    @IBOutlet var macAddressTextField : UITextField!
    @IBOutlet var userNameTextField : UITextField!
    @IBOutlet var passwordTextField : UITextField!
    @IBOutlet weak var lastRequestStatus: UILabel!
    
    var macAddress = TEST_DEVICE_MAC_ADDRESS
    
    /// <#Description#>
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        startupInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchCMXMotionUpdates()
    }
    
    /// <#Description#>
    func setupForNavigationBar(){
    }
    
    /// <#Description#>
    func registerForNotifications(){
    }
    
    /// <#Description#>
    func startupInitialisations(){
        if let cachedText = UserDefaults.standard.object(forKey: "cmxAccessUsername") as? String{
            if cachedText.length > 0 {
                userNameTextField.text = cachedText
                CMX_ACCESS_USER_NAME = cachedText
            }else{
                userNameTextField.text = CMX_ACCESS_USER_NAME
            }
        }else{
            userNameTextField.text = CMX_ACCESS_USER_NAME
        }
        if let cachedText = UserDefaults.standard.object(forKey: "cmxAccessPassword") as? String{
            if cachedText.length > 0 {
                passwordTextField.text = cachedText
                CMX_ACCESS_PASSWORD = cachedText
            }else{
                passwordTextField.text = CMX_ACCESS_PASSWORD
            }
        }else{
            passwordTextField.text = CMX_ACCESS_PASSWORD
        }
        if let cachedText = UserDefaults.standard.object(forKey: "cmxBaseUrl") as? String{
            if cachedText.length > 0 {
                baseUrlTextField.text = cachedText
                BASE_URL = cachedText
            }else{
                baseUrlTextField.text = BASE_URL
            }
        }else{
            baseUrlTextField.text = BASE_URL
        }
        if let cachedText = UserDefaults.standard.object(forKey: "observerMacAddress") as? String{
            macAddressTextField.text = cachedText
        }else{
            macAddressTextField.text = TEST_DEVICE_MAC_ADDRESS
        }
        
        fetchCMXMotionUpdates()
    }
    
    func fetchCMXMotionUpdates(){
        VSCMXController.sharedInstance.startCMXMotionUpdates(macAddress: macAddressTextField.text!) { (locationUpdate) in
            if isNotNull(locationUpdate){
                self.updateCMXLocationDataOnScreen(locationUpdate: locationUpdate!)
                self.lastRequestStatus.text = "Location Update Fetched"
                self.lastRequestStatus.textColor = UIColor.green
            }else{
                self.lastRequestStatus.text = "Device with above Mac Address is Not Reachable"
                if isInternetConnectivityAvailable(false) == false {
                    self.lastRequestStatus.text = MESSAGE_TEXT___FOR_NETWORK_NOT_REACHABILITY
                }
                self.lastRequestStatus.textColor = UIColor.red
            }
            self.cmxLocationTableView.reloadData()
        }
    }
    
    /// <#Description#>
    ///
    /// - parameter date: <#date description#>
    ///
    /// - returns: <#return value description#>
    func dateStringForDate(date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .medium
        return dateFormatter.string(from: date)
    }
    
    /// <#Description#>
    ///
    /// - parameter locationUpdate: <#locationUpdate description#>
    func updateCMXLocationDataOnScreen(locationUpdate:VSCMXLocationUpdate) {
        cmxLocationXCoordinateLabel.text = "\(String(format: "%.3f",locationUpdate.x))"
        cmxLocationYCoordinateLabel.text = "\(String(format: "%.3f",locationUpdate.y))"
        cmxLocationZCoordinateLabel.text = "\(String(format: "%.3f",locationUpdate.z))"
        cmxLocationServerTimeStamp.text = "\(locationUpdate.serverTimeStamp)"
        cmxLocationLocalTimeStamp.text = "\(dateStringForDate(date: locationUpdate.capturedAt))"
        if VSDataManager.sharedInstance.cmxLocationLatestDataToDisplay.count < KMaxObservationCount {
            VSDataManager.sharedInstance.cmxLocationLatestDataToDisplay.add(locationUpdate)
        }else{
            VSDataManager.sharedInstance.cmxLocationLatestDataToDisplay.removeObject(at: 0)
            VSDataManager.sharedInstance.cmxLocationLatestDataToDisplay.add(locationUpdate)
        }
    }
    
    /// <#Description#>
    func updateUserInterfaceOnScreen(){
        
    }
    
    /// <#Description#>
    ///
    /// - parameter tableView: <#tableView description#>
    /// - parameter section:   <#section description#>
    ///
    /// - returns: <#return value description#>
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return VSDataManager.sharedInstance.cmxLocationLatestDataToDisplay.count
    }
    
    
    /// <#Description#>
    ///
    /// - parameter tableView: <#tableView description#>
    /// - parameter indexPath: <#indexPath description#>
    ///
    /// - returns: <#return value description#>
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let locationUpdateRepresentingCell = tableView.dequeueReusableCell(withIdentifier: "CMXLocationUpdateRepresentingCell", for: indexPath as IndexPath) as! CMXLocationUpdateRepresentingCell
        let locationUpdate = VSDataManager.sharedInstance.cmxLocationLatestDataToDisplay.reversed()[ indexPath.row] as! VSCMXLocationUpdate
        locationUpdateRepresentingCell.infoLabel4.text = "\(locationUpdate.capturedAt.timePassed())"
        locationUpdateRepresentingCell.infoLabel1.text = "X : \(locationUpdate.x)"
        locationUpdateRepresentingCell.infoLabel2.text = "Y : \(locationUpdate.y)"
        locationUpdateRepresentingCell.infoLabel3.text = "Z : \(locationUpdate.z)"
        return locationUpdateRepresentingCell
    }
    
    /// <#Description#>
    ///
    /// - parameter textField: <#textField description#>
    ///
    /// - returns: <#return value description#>
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        fetchCMXMotionUpdates()
        return true
    }
    
    /// <#Description#>
    ///
    /// - parameter textField: <#textField description#>
    func textFieldDidEndEditing(_ textField: UITextField) {
        macAddressTextField.text = macAddressTextField.text?.trimmedString()
        macAddress = macAddressTextField.text ?? TEST_DEVICE_MAC_ADDRESS
        if let text = macAddressTextField.text as? NSString {
            UserDefaults.standard.set(text, forKey: "observerMacAddress")
        }
        CMX_ACCESS_USER_NAME = userNameTextField.text ?? CMX_ACCESS_USER_NAME
        if let text = userNameTextField.text as? NSString {
            UserDefaults.standard.set(text, forKey: "cmxAccessUsername")
        }
        CMX_ACCESS_PASSWORD = passwordTextField.text ?? CMX_ACCESS_PASSWORD
        if let text = passwordTextField.text as? NSString {
            UserDefaults.standard.set(text, forKey: "cmxAccessPassword")
        }
        if baseUrlTextField.text != nil {
            if (NSURL(string: baseUrlTextField.text!)?.host as? NSString) != nil {
                BASE_URL = baseUrlTextField.text ?? BASE_URL
                if let text = baseUrlTextField.text as? NSString {
                    UserDefaults.standard.set(text, forKey: "cmxBaseUrl")
                }
            }else{
                showAlert("Invalid Url 😡", message: "It seems the url is invalid .. Please try again")
                performOnMainThreadWithOptimisation({ (ok) in
                    showAlert("Instructions 🤓", message: "A valid url should look like \nhttp://128.107.3.20/\nit must start with http:// and end with '/'")
                },delay: 3)
                baseUrlTextField.text = BASE_URL
            }
        }
    }
    /// <#Description#>
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

