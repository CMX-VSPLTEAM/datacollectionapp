//
//  VSInformationViewController.swift
//  VSensor
//
//  Created by Virinchi Softwares on 14/09/16.
//  Copyright © 2016 Virinchi Softwares. All rights reserved.
//


import UIKit
import Foundation

/// <#Description#>
class VSInformationViewController: UIViewController {
    
    /// <#Description#>
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        startupInitialisations()
        setupForNavigationBar()
        registerForNotifications()
        updateUserInterfaceOnScreen()
    }
    
    
    /// <#Description#>
    func setupForNavigationBar(){
    }
    
    /// <#Description#>
    func registerForNotifications(){
    }
    
    /// <#Description#>
    func startupInitialisations(){
    }
    
    /// <#Description#>
    func updateUserInterfaceOnScreen(){
        
    }
    
    @IBAction func onClickOfCloseButton() {
        self.dismiss(animated: true, completion: nil)
    }
    
    /// <#Description#>
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

