//
//  VSMotionActivity.swift
//  VSensor
//
//  Created by Virinchi Softwares on 14/09/16.
//  Copyright © 2016 Virinchi Softwares. All rights reserved.
//

import Foundation
import CoreMotion

/// Data modal to hold motionActivity
class VSMotionActivity: VSModalBase {
    
    open var motionActivity :CMMotionActivity!
    open var capturedAt = Date()
    
    /// <#Description#>
    ///
    /// - parameter motionActivity: <#motionActivity description#>
    /// - parameter capturedAt:     <#capturedAt description#>
    ///
    /// - returns: <#return value description#>
    init(motionActivity:CMMotionActivity,capturedAt:Date?) {
        self.motionActivity = motionActivity
        if capturedAt != nil {
            self.capturedAt = capturedAt!
        }
    }
}
