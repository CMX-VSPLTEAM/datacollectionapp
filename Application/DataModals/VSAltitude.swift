//
//  VSAltitude.swift
//  VSensor
//
//  Created by Virinchi Softwares on 14/09/16.
//  Copyright © 2016 Virinchi Softwares. All rights reserved.
//

import Foundation
import CoreMotion

/// Data modal to hold altitude
class VSAltitude: VSModalBase {
    
    open var altitide : CMAltitudeData!
    open var capturedAt = Date()
    
    /// <#Description#>
    ///
    /// - parameter altitide:   <#altitide description#>
    /// - parameter capturedAt: <#capturedAt description#>
    ///
    /// - returns: <#return value description#>
    init(altitide:CMAltitudeData,capturedAt:Date?) {
        self.altitide = altitide
        if capturedAt != nil {
            self.capturedAt = capturedAt!
        }
    }
}
