//
//  VSModalBase.swift
//  VSensor
//
//  Created by Virinchi Softwares on 14/09/16.
//  Copyright © 2016 Virinchi Softwares. All rights reserved.
//

import Foundation
import CoreMotion

/// <#Description#>
class VSModalBase: NSObject {
    
    open var userName = "Guest"
    open var userAction = "Stationary"
    open var toRecord = false
    
    /// <#Description#>
    ///
    /// - returns: <#return value description#>
    override init() {
        userName = VSDataManager.sharedInstance.observerName
        userAction = VSDataManager.sharedInstance.observerAction
        toRecord = VSDataManager.sharedInstance.isRecording
    }
}
