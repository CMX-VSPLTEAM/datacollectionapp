//
//  VSAttitude.swift
//  VSensor
//
//  Created by Virinchi Softwares on 14/09/16.
//  Copyright © 2016 Virinchi Softwares. All rights reserved.
//

import Foundation

/// Data modal to hold attitude
class VSAttitude: VSModalBase {
    
    open var yaw = 0.0
    open var roll = 0.0
    open var pitch = 0.0
    open var capturedAt = Date()
    
    /// Init
    ///
    /// - parameter yaw:        yaw value
    /// - parameter roll:       roll value
    /// - parameter pitch:      pitch value
    /// - parameter capturedAt: capturedAt value
    ///
    /// - returns: return value description
    init(yaw:Double,roll:Double,pitch:Double,capturedAt:Date?) {
        self.yaw = yaw
        self.roll = roll
        self.pitch = pitch
        if capturedAt != nil {
            self.capturedAt = capturedAt!
        }
    }
}
