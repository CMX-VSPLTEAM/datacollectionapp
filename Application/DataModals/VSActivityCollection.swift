//
//  VSActivityCollection.swift
//  VSensor
//
//  Created by Virinchi Softwares on 14/09/16.
//  Copyright © 2016 Virinchi Softwares. All rights reserved.
//

import Foundation
import CoreMotion

/// To distinguish various types of motion
///
/// - Cycling: motion activity due to cycling
/// - Running: motion activity due to running
/// - Walking: motion activity due to walking
/// - Other:   motion activity due to anything else
enum ActivityType {
  case Cycling
  case Running
  case Walking
  case Other
}

/// an activity
struct Activity {

  let type: ActivityType
  let startDate: NSDate
  let endDate: NSDate
  
  /// <#Description#>
  ///
  /// - parameter motionActivity: <#motionActivity description#>
  ///
  /// - returns: <#return value description#>
  init(motionActivity: CMMotionActivity) {
    if motionActivity.cycling {
      type = .Cycling
    } else if motionActivity.running {
      type = .Running
    } else if motionActivity.walking {
      type = .Walking
    } else {
      type = .Other
    }
    startDate = motionActivity.startDate as NSDate
    endDate = motionActivity.startDate as NSDate
  }
  
  /// <#Description#>
  ///
  /// - parameter activity:   <#activity description#>
  /// - parameter newEndDate: <#newEndDate description#>
  ///
  /// - returns: <#return value description#>
  init(activity: Activity, newEndDate: NSDate) {
    type = activity.type
    startDate = activity.startDate
    endDate = newEndDate
  }
  
  /// <#Description#>
  ///
  /// - parameter activity: <#activity description#>
  ///
  /// - returns: <#return value description#>
  func appendActivity(activity: Activity) -> Activity {
    return Activity(activity: self, newEndDate: activity.endDate)
  }
  
}


/// Collection of activities
class VSActivityCollection {
  var activities = [Activity]()

  /// <#Description#>
  ///
  /// - parameter activities: <#activities description#>
  ///
  /// - returns: <#return value description#>
  init(activities: [CMMotionActivity]) {
    addMotionActivities(motionActivities: activities)
  }
  
  /// <#Description#>
  ///
  /// - parameter motionActivity: <#motionActivity description#>
  func addMotionActivity(motionActivity: CMMotionActivity) {
    var activity = Activity(motionActivity: motionActivity)    
    if activities.last?.type == activity.type {
      activity = activities.last!.appendActivity(activity: activity)
      activities.removeLast()
    }
    if (activity.type != .Other) {
      activities.append(activity)
    }
  }
  
  /// <#Description#>
  ///
  /// - parameter motionActivities: <#motionActivities description#>
  func addMotionActivities(motionActivities: [CMMotionActivity]) {
    for activity in motionActivities {
      addMotionActivity(motionActivity: activity)
    }
  }
}


