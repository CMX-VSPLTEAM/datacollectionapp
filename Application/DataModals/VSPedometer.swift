//
//  VSPedometer.swift
//  VSensor
//
//  Created by Virinchi Softwares on 14/09/16.
//  Copyright © 2016 Virinchi Softwares. All rights reserved.
//

import Foundation
import CoreMotion

/// Data modal to hold pedometer
class VSPedometer: VSModalBase {
    
    open var pedometer : CMPedometerData!
    open var capturedAt = Date()
    
    /// <#Description#>
    ///
    /// - parameter pedometer:  <#pedometer description#>
    /// - parameter capturedAt: <#capturedAt description#>
    ///
    /// - returns: <#return value description#>
    init(pedometer:CMPedometerData,capturedAt:Date?) {
        self.pedometer = pedometer
        if capturedAt != nil {
            self.capturedAt = capturedAt!
        }
    }
}
