//
//  VSMagneticField.swift
//  VSensor
//
//  Created by Virinchi Softwares on 14/09/16.
//  Copyright © 2016 Virinchi Softwares. All rights reserved.
//

import Foundation

/// Data modal to hold magnetic field
class VSMagneticField: VSModalBase {
    
    open var x = 0.0
    open var y = 0.0
    open var z = 0.0
    open var capturedAt = Date()
    
    /// Init
    ///
    /// - parameter x:          x value
    /// - parameter y:          y value
    /// - parameter z:          z value
    /// - parameter capturedAt: capturedAt value
    ///
    /// - returns: newly create object
    init(x:Double,y:Double,z:Double,capturedAt:Date?) {
        self.x = x
        self.y = y
        self.z = z
        if capturedAt != nil {
            self.capturedAt = capturedAt!
        }
    }
}
