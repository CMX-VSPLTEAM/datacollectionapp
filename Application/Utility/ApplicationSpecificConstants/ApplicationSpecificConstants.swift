//
//  ApplicationSpecificConstants.h
//  Application
//
//  Created by Virinchi Softwares on 28/09/16.
//  Copyright (c) 2016 Virinchi Softwares. All rights reserved.
//

import UIKit

// constants strings
let SUPPORT_EMAIL = "support@application.com"
let GOOGLE_API_KEY = "AIzaSyCzHX7ice9Aq1H1MjWGZkHkruNQ0XigUB0"
let APP_NAME = "Application"
let DEVICE_TYPE = "iOS"
let FONT_BOLD = "MavenProBold"
let FONT_SEMI_BOLD = "MavenProMedium"
let FONT_REGULAR = "MavenProRegular"
let APP_UNIQUE_BUILD_IDENTIFIER = "APP_UNIQUE_BUILD_IDENTIFIER"


// constants keys


// constants cache manager keys


// constants values
let ENABLE_LOGGING_WEB_SERVICE_RESPONSE = true
let MINOR_DELAY = 0.1
let PAGE_SIZE = 10
let AUTO_DISMISS_ALERTS_MESSAGE_DURATION = 20.0
let MINIMUM_LENGTH_LIMIT_USERNAME = 1
let MAXIMUM_LENGTH_LIMIT_USERNAME = 32
let MINIMUM_LENGTH_LIMIT_FIRST_NAME = 0
let MAXIMUM_LENGTH_LIMIT_FIRST_NAME = 64
let MINIMUM_LENGTH_LIMIT_PASSWORD = 1
let MAXIMUM_LENGTH_LIMIT_PASSWORD = 20
let MINIMUM_LENGTH_LIMIT_MOBILE_NUMBER = 7
let MAXIMUM_LENGTH_LIMIT_MOBILE_NUMBER = 14
let MINIMUM_LENGTH_LIMIT_EMAIL = 7
let MAXIMUM_LENGTH_LIMIT_EMAIL = 64
let REQUEST_TIME_OUT = 90.0
var MOTION_UPDATE_INTERVAL = 0.3
var MOTION_UPDATE_INTERVAL_MINIMUM_VALUE = 0.2
var CMX_MOTION_UPDATE_INTERVAL = 5
let TEST_DEVICE_MAC_ADDRESS = "a4:08:ea:8c:01:56"
var CMX_ACCESS_USER_NAME = "sdktest"
var CMX_ACCESS_PASSWORD = "sdktest"


// constant feature enable/disable flags
var ENABLE_CRASHLITICS = true

// constant urls

// constant feature enable/disable flags

// constant urls
let APP_FACEBOOK_PAGE_LINK = "https://www.facebook.com/ridewithVirinchi Softwares/?fref=ts"
let APP_TWITTER_PAGE_LINK = "https://twitter.com/ridewithVirinchi Softwares"
let APP_LINKEDIN_PAGE_LINK = "https://www.linkedin.com/company/4844251"
let APP_WEBSITE_LINK = "https://www.Virinchi Softwares.com/"
let APP_ABOUT_US_PAGE_LINK = "https://www.Virinchi Softwares.com/?page_id=2482"
let APP_FAQ_PAGE_LINK = "https://www.Virinchi Softwares.com/?page_id=2566"
let APP_CONTACT_US_PAGE_LINK = "https://www.Virinchi Softwares.com/#contact-us"
let APP_TERMS_OF_USE_AND_PRIVACY_POLICY = "https://www.Virinchi Softwares.com/?page_id=2554"

// constants objects
let userDefaults = UserDefaults.standard
let appDelegate = (UIApplication.shared.delegate as! AppDelegate)
let KDeviceWidth  = UIScreen.main.bounds.size.width as CGFloat
let KDeviceHeight = UIScreen .main.bounds.size.height as CGFloat
let KMaxAccelerationValue = 1.0 as CGFloat
let KMaxObservationCount = 4000
let KPositiveColorValue = UIColor(red: 96/255, green: 196/255, blue: 221/255, alpha: 0.95)
let KNegativeColorValue = UIColor(red: 230/255, green: 112/255, blue: 99/255, alpha: 1.0)

// constants color
let APP_THEME_COLOR = UIColor(red: 75/255, green: 19/255, blue: 82/255, alpha: 1.0)
let APP_THEME_RED_COLOR = UIColor(red: 230/255, green: 112/255, blue: 99/255, alpha: 1.0)

// constants descriptions
let MESSAGE_TEXT___FOR_NETWORK_NOT_REACHABILITY = "The Internet connection appears to be offline."
let MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY = "Connection failed!. Please try again!"
let MESSAGE_TEXT___FOR_FUNCTIONALLITY_PENDING_MESSAGE = "We are still developing this feature. Thanks for your patience"


// constant notifications
