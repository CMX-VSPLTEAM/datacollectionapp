//
//  VSDataManager.swift
//  VSensor
//
//  Created by Virinchi Softwares on 14/09/16.
//  Copyright © 2016 Virinchi Softwares. All rights reserved.
//

import Foundation
import CoreMotion
import MessageUI

class VSDataManager: NSObject , MFMailComposeViewControllerDelegate {
    
    var attitudeLatestDataToDisplay = NSMutableArray()
    var magneticFieldLatestDataToDisplay = NSMutableArray()
    var rotationLatestDataToDisplay = NSMutableArray()
    var accelerometerLatestDataToDisplay = NSMutableArray()
    var activityLatestDataToDisplay = NSMutableArray()
    var pedoMeterLatestDataToDisplay = NSMutableArray()
    var altitudeLatestDataToDisplay = NSMutableArray()
    var cmxLocationLatestDataToDisplay = NSMutableArray()
    var observerName = "Guest"
    var observerAction = "Stationary"
    var isRecording = false
    
    /// <#Description#>
    static let sharedInstance : VSDataManager = {
        let instance = VSDataManager()
        return instance
    }()
    
    fileprivate override init() {
        
    }
    
    /// <#Description#>
    ///
    /// - parameter date: <#date description#>
    ///
    /// - returns: <#return value description#>
    func dateStringForDate(date:Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .medium
        return dateFormatter.string(from: date).replacingOccurrences(of: ",", with: " ")
    }
    
    func currentTimeStamp() -> Int64{
        return Int64(NSDate().timeIntervalSince1970*1000)
    }
    
    /// <#Description#>
    func share(){
        let fileNamePrefix = "\(currentTimeStamp())_"
        
        //acceleration
        var accelerationCSVFilePath = ""
        let accelerationfileName = "\(fileNamePrefix)Acceleration.csv"
        if VSDataManager.sharedInstance.accelerometerLatestDataToDisplay.count > 0 {
            let keys = ["Observation Type","X","Y","Z","TimeStamp (milli seconds)","Observer Name","Observer Action"]
            let observationTypeArray = NSMutableArray()
            let xValues = NSMutableArray()
            let yValues = NSMutableArray()
            let zValues = NSMutableArray()
            let timeStamp = NSMutableArray()
            let name = NSMutableArray()
            let action = NSMutableArray()
            for i in 0...VSDataManager.sharedInstance.accelerometerLatestDataToDisplay.count-1 {
                let acceleration = VSDataManager.sharedInstance.accelerometerLatestDataToDisplay.object(at: i) as! VSAccelerate
                if acceleration.toRecord {
                    observationTypeArray.add("acceleration (g)")
                    xValues.add("\(acceleration.x)")
                    yValues.add("\(acceleration.y)")
                    zValues.add("\(acceleration.z)")
                    timeStamp.add("\(acceleration.capturedAt.timeIntervalSince1970*1000)")
                    name.add(acceleration.userName)
                    action.add(acceleration.userAction)
                }
            }
            if observationTypeArray.count > 0 {
                accelerationCSVFilePath = VSCSVExporter.exportToCsv(accelerationfileName as NSString, keyArray: keys as NSArray, variableArray: [observationTypeArray,xValues,yValues,zValues,timeStamp,name,action])
            }
        }
        
        //rotation
        var rotationCSVFilePath = ""
        let rotationfileName = "\(fileNamePrefix)Rotation.csv"
        if VSDataManager.sharedInstance.rotationLatestDataToDisplay.count > 0 {
            let keys = ["Observation Type","X","Y","Z","TimeStamp (milli seconds)","Observer Name","Observer Action"]
            let observationTypeArray = NSMutableArray()
            let xValues = NSMutableArray()
            let yValues = NSMutableArray()
            let zValues = NSMutableArray()
            let timeStamp = NSMutableArray()
            let name = NSMutableArray()
            let action = NSMutableArray()
            for i in 0...VSDataManager.sharedInstance.rotationLatestDataToDisplay.count-1 {
                let rotation = VSDataManager.sharedInstance.rotationLatestDataToDisplay.object(at: i) as! VSRotation
                if rotation.toRecord {
                    observationTypeArray.add("rotation (radian/sec)")
                    xValues.add("\(rotation.x)")
                    yValues.add("\(rotation.y)")
                    zValues.add("\(rotation.z)")
                    timeStamp.add("\(rotation.capturedAt.timeIntervalSince1970*1000)")
                    name.add(rotation.userName)
                    action.add(rotation.userAction)
                }
            }
            if observationTypeArray.count > 0 {
                rotationCSVFilePath = VSCSVExporter.exportToCsv(rotationfileName as NSString, keyArray: keys as NSArray, variableArray: [observationTypeArray,xValues,yValues,zValues,timeStamp,name,action])
            }
        }
        
        
        //magnetic field
        var magneticFieldCSVFilePath = ""
        let magneticFieldfileName = "\(fileNamePrefix)MagneticField.csv"
        if VSDataManager.sharedInstance.magneticFieldLatestDataToDisplay.count > 0 {
            let keys = ["Observation Type","X","Y","Z","TimeStamp (milli seconds)","Observer Name","Observer Action"]
            let observationTypeArray = NSMutableArray()
            let xValues = NSMutableArray()
            let yValues = NSMutableArray()
            let zValues = NSMutableArray()
            let timeStamp = NSMutableArray()
            let name = NSMutableArray()
            let action = NSMutableArray()
            for i in 0...VSDataManager.sharedInstance.magneticFieldLatestDataToDisplay.count-1 {
                let magneticField = VSDataManager.sharedInstance.magneticFieldLatestDataToDisplay.object(at: i) as! VSMagneticField
                if magneticField.toRecord {
                    observationTypeArray.add("magnetic field (μT)")
                    xValues.add("\(magneticField.x)")
                    yValues.add("\(magneticField.y)")
                    zValues.add("\(magneticField.z)")
                    timeStamp.add("\(magneticField.capturedAt.timeIntervalSince1970*1000)")
                    name.add(magneticField.userName)
                    action.add(magneticField.userAction)
                }
            }
            if observationTypeArray.count > 0 {
                magneticFieldCSVFilePath = VSCSVExporter.exportToCsv(magneticFieldfileName as NSString, keyArray: keys as NSArray, variableArray: [observationTypeArray,xValues,yValues,zValues,timeStamp,name,action])
            }
        }
        
        //Attitude
        var attitudeCSVFilePath = ""
        let attitudefileName = "\(fileNamePrefix)Attitude.csv"
        if VSDataManager.sharedInstance.attitudeLatestDataToDisplay.count > 0 {
            let keys = ["Observation Type","Yaw","Pitch","Roll","TimeStamp (milli seconds)","Observer Name","Observer Action"]
            let observationTypeArray = NSMutableArray()
            let xValues = NSMutableArray()
            let yValues = NSMutableArray()
            let zValues = NSMutableArray()
            let timeStamp = NSMutableArray()
            let name = NSMutableArray()
            let action = NSMutableArray()
            for i in 0...VSDataManager.sharedInstance.attitudeLatestDataToDisplay.count-1 {
                let attitude = VSDataManager.sharedInstance.attitudeLatestDataToDisplay.object(at: i) as! VSAttitude
                if attitude.toRecord {
                    observationTypeArray.add("Attitude (Degree)")
                    xValues.add("\(attitude.yaw)")
                    yValues.add("\(attitude.pitch)")
                    zValues.add("\(attitude.roll)")
                    timeStamp.add("\(attitude.capturedAt.timeIntervalSince1970*1000)")
                    name.add(attitude.userName)
                    action.add(attitude.userAction)
                }
            }
            if observationTypeArray.count > 0 {
                attitudeCSVFilePath = VSCSVExporter.exportToCsv(attitudefileName as NSString, keyArray: keys as NSArray, variableArray: [observationTypeArray,xValues,yValues,zValues,timeStamp,name,action])
            }
        }
        
        //Activity
        var activityCSVFilePath = ""
        let activityFileName = "\(fileNamePrefix)Activity.csv"
        if VSDataManager.sharedInstance.activityLatestDataToDisplay.count > 0 {
            let keys = ["Observation Type","Activity Type","TimeStamp (milli seconds)","Observer Name","Observer Action"]
            let observationTypeArray = NSMutableArray()
            let activityType = NSMutableArray()
            let timeStamp = NSMutableArray()
            let name = NSMutableArray()
            let action = NSMutableArray()
            for i in 0...VSDataManager.sharedInstance.activityLatestDataToDisplay.count-1 {
                let motionActivity = VSDataManager.sharedInstance.activityLatestDataToDisplay.object(at: i) as! VSMotionActivity
                if motionActivity.toRecord {
                    observationTypeArray.add("Activity")
                    if motionActivity.motionActivity.running {
                        activityType.add("Running")
                    } else if motionActivity.motionActivity.cycling {
                        activityType.add("Cycling")
                    } else if motionActivity.motionActivity.walking {
                        activityType.add("Walking")
                    }else if motionActivity.motionActivity.automotive {
                        activityType.add("Automotive")
                    }else if motionActivity.motionActivity.stationary {
                        activityType.add("Stationary")
                    }else {
                        activityType.add("Unknown")
                    }
                    timeStamp.add("\(motionActivity.capturedAt.timeIntervalSince1970*1000)")
                    name.add(motionActivity.userName)
                    action.add(motionActivity.userAction)
                }
            }
            if observationTypeArray.count > 0 {
                activityCSVFilePath = VSCSVExporter.exportToCsv(activityFileName as NSString, keyArray: keys as NSArray, variableArray: [observationTypeArray,activityType,timeStamp,name,action])
            }
        }
        
        //Pedometer
        var pedometerCSVFilePath = ""
        let pedometerFileName = "\(fileNamePrefix)Pedometer.csv"
        if VSDataManager.sharedInstance.pedoMeterLatestDataToDisplay.count > 0 {
            let keys = ["Observation Type","Steps","Floors","Distance (meters)","TimeStamp (milli seconds)","Observer Name","Observer Action"]
            let observationTypeArray = NSMutableArray()
            let steps = NSMutableArray()
            let floors = NSMutableArray()
            let distance = NSMutableArray()
            let timeStamp = NSMutableArray()
            let name = NSMutableArray()
            let action = NSMutableArray()
            for i in 0...VSDataManager.sharedInstance.pedoMeterLatestDataToDisplay.count-1 {
                let pedometer = VSDataManager.sharedInstance.pedoMeterLatestDataToDisplay.object(at: i) as! VSPedometer
                if pedometer.toRecord {
                    observationTypeArray.add("Pedometer")
                    steps.add("\(safeInt(object: pedometer.pedometer.numberOfSteps, alternate: 0))")
                    floors.add("\(safeInt(object: pedometer.pedometer.floorsAscended, alternate: 0))")
                    distance.add("\(safeDouble(object: pedometer.pedometer.distance, alternate: 0))")
                    timeStamp.add("\(pedometer.capturedAt.timeIntervalSince1970*1000)")
                    name.add(pedometer.userName)
                    action.add(pedometer.userAction)
                }
            }
            if observationTypeArray.count > 0 {
                pedometerCSVFilePath = VSCSVExporter.exportToCsv(pedometerFileName as NSString, keyArray: keys as NSArray, variableArray: [observationTypeArray,steps,floors,distance,timeStamp,name,action])
            }
        }
        
        //Altitude
        var altitudeCSVFilePath = ""
        let altitudeFileName = "\(fileNamePrefix)Altitude.csv"
        if VSDataManager.sharedInstance.altitudeLatestDataToDisplay.count > 0 {
            let keys = ["Observation Type","Relative Altitude (meters)","TimeStamp (milli seconds)","Observer Name","Observer Action"]
            let observationTypeArray = NSMutableArray()
            let relativeAltitudeArray = NSMutableArray()
            let timeStamp = NSMutableArray()
            let name = NSMutableArray()
            let action = NSMutableArray()
            for i in 0...VSDataManager.sharedInstance.altitudeLatestDataToDisplay.count-1 {
                let altitude = VSDataManager.sharedInstance.altitudeLatestDataToDisplay.object(at: i) as! VSAltitude
                if altitude.toRecord {
                    observationTypeArray.add("Altitude")
                    relativeAltitudeArray.add("\(altitude.altitide.relativeAltitude)")
                    timeStamp.add("\(altitude.capturedAt.timeIntervalSince1970*1000)")
                    name.add(altitude.userName)
                    action.add(altitude.userAction)
                }
            }
            if observationTypeArray.count > 0 {
                altitudeCSVFilePath = VSCSVExporter.exportToCsv(altitudeFileName as NSString, keyArray: keys as NSArray, variableArray: [observationTypeArray,relativeAltitudeArray,timeStamp,name,action])
            }
        }
        
        //CMXLocationUpdate
        var cmxLocationUpdateCSVFilePath = ""
        let cmxLocationUpdateFileName = "\(fileNamePrefix)CMXLocationUpdate.csv"
        if VSDataManager.sharedInstance.cmxLocationLatestDataToDisplay.count > 0 {
            let keys = ["Observation Type","X","Y","Z","MacAddress","Floor Id","TimeStamp (milli seconds)","Server TimeStamp","Observer Name","Observer Action"]
            let observationTypeArray = NSMutableArray()
            let xValues = NSMutableArray()
            let yValues = NSMutableArray()
            let zValues = NSMutableArray()
            let macAddress = NSMutableArray()
            let floorId = NSMutableArray()
            let timeStamp = NSMutableArray()
            let serverTimeStamp = NSMutableArray()
            let name = NSMutableArray()
            let action = NSMutableArray()
            for i in 0...VSDataManager.sharedInstance.cmxLocationLatestDataToDisplay.count-1 {
                let cmxLocationUpdate = VSDataManager.sharedInstance.cmxLocationLatestDataToDisplay.object(at: i) as! VSCMXLocationUpdate
                if cmxLocationUpdate.toRecord {
                    observationTypeArray.add("location (feet)")
                    xValues.add("\(cmxLocationUpdate.x)")
                    yValues.add("\(cmxLocationUpdate.y)")
                    zValues.add("\(cmxLocationUpdate.z)")
                    macAddress.add("\(cmxLocationUpdate.deviceMACAddress)")
                    floorId.add("\(cmxLocationUpdate.floorId)")
                    timeStamp.add("\(cmxLocationUpdate.capturedAt.timeIntervalSince1970*1000)")
                    serverTimeStamp.add("\(cmxLocationUpdate.serverTimeStamp)")
                    name.add(cmxLocationUpdate.userName)
                    action.add(cmxLocationUpdate.userAction)
                }
            }
            if observationTypeArray.count > 0 {
                cmxLocationUpdateCSVFilePath = VSCSVExporter.exportToCsv(cmxLocationUpdateFileName as NSString, keyArray: keys as NSArray, variableArray: [observationTypeArray,xValues,yValues,zValues,macAddress,floorId,timeStamp,serverTimeStamp,name,action])
            }
        }
        
        //check mail confifuration setup in device. Simulator can't send email
        if MFMailComposeViewController.canSendMail() {
            let mailComposer = MFMailComposeViewController()
            mailComposer.mailComposeDelegate = self
            mailComposer.setSubject("Observation : VSensor")
            mailComposer.setMessageBody("Hello,\n\nWe have attached collected data from the VSensor app. Please find the following files attached. \n1: \(accelerationfileName)\n2: \(rotationfileName)\n3: \(magneticFieldfileName)\n4: \(attitudefileName)\n5: \(activityFileName)\n6: \(pedometerFileName)\n7: \(altitudeFileName)\n8: \(cmxLocationUpdateFileName)", isHTML: false)
            if let data = NSData(contentsOfFile:accelerationCSVFilePath) {
                mailComposer.addAttachmentData(data as Data, mimeType: "text/csv", fileName: accelerationfileName)
            }
            if let data = NSData(contentsOfFile:rotationCSVFilePath) {
                mailComposer.addAttachmentData(data as Data, mimeType: "text/csv", fileName: rotationfileName)
            }
            if let data = NSData(contentsOfFile:magneticFieldCSVFilePath) {
                mailComposer.addAttachmentData(data as Data, mimeType: "text/csv", fileName: magneticFieldfileName)
            }
            if let data = NSData(contentsOfFile:attitudeCSVFilePath) {
                mailComposer.addAttachmentData(data as Data, mimeType: "text/csv", fileName: attitudefileName)
            }
            if let data = NSData(contentsOfFile:activityCSVFilePath) {
                mailComposer.addAttachmentData(data as Data, mimeType: "text/csv", fileName: activityFileName)
            }
            if let data = NSData(contentsOfFile:pedometerCSVFilePath) {
                mailComposer.addAttachmentData(data as Data, mimeType: "text/csv", fileName: pedometerFileName)
            }
            if let data = NSData(contentsOfFile:altitudeCSVFilePath) {
                mailComposer.addAttachmentData(data as Data, mimeType: "text/csv", fileName: altitudeFileName)
            }
            if let data = NSData(contentsOfFile:cmxLocationUpdateCSVFilePath) {
                mailComposer.addAttachmentData(data as Data, mimeType: "text/csv", fileName: cmxLocationUpdateFileName)
            }
            windowObject()?.rootViewController?.present(mailComposer, animated: true, completion: nil)
        }
    }
    
    
    /// <#Description#>
    ///
    /// - returns: <#return value description#>
    public func storyBoardObject()->(UIStoryboard){
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
    /// <#Description#>
    ///
    /// - parameter identifier: <#identifier description#>
    ///
    /// - returns: <#return value description#>
    func viewController(identifier:NSString)->(UIViewController){
        return storyBoardObject().instantiateViewController(withIdentifier: identifier as String)
    }
    
    ///
    ///
    /// - parameter controller: MFMailComposeViewController instance
    /// - parameter result:     MFMailComposeResult options
    /// - parameter error:      error
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?){
        controller.dismiss(animated: true, completion: nil)
    }
    
    /// <#Description#>
    ///
    /// - returns: <#return value description#>
    func windowObject ()->UIWindow?{
        return UIApplication.shared.keyWindow
    }
}
