//
//  VSCMXController.swift
//  VSensor
//
//  Created by Virinchi Softwares on 14/09/16.
//  Copyright © 2016 Virinchi Softwares. All rights reserved.
//

import Foundation
import CoreMotion


/// Call back handlers , block based
typealias VSCMXCompletionBlock = (_ update:VSCMXLocationUpdate?) ->()

/// Manager class dealling with every type of motion events
class VSCMXController: NSObject {
    
    var completion : VSCMXCompletionBlock?
    var macAddress = TEST_DEVICE_MAC_ADDRESS
    /// initialize instance
    static let sharedInstance : VSCMXController = {
        let instance = VSCMXController()
        return instance
    }()
    
    fileprivate override init() {
        
    }
    
    /// <#Description#>
    ///
    /// - parameter completion: <#completion description#>
    func startCMXMotionUpdates(macAddress:String,_ completion:@escaping VSCMXCompletionBlock) {
        self.macAddress = macAddress
        self.completion = completion
        reScheduleUpdate()
    }
    
    func reScheduleUpdate(){
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(VSCMXController.fetchLocationDataFromCMX), object: nil)
        self.perform(#selector(VSCMXController.fetchLocationDataFromCMX), with: nil, afterDelay: TimeInterval(CMX_MOTION_UPDATE_INTERVAL))
    }
    
    func fetchLocationDataFromCMX(){
        if isInternetConnectivityAvailable(false) == false{
            reScheduleUpdate()
            self.completion!(nil)
        }else{
            let information = NSMutableDictionary()
            information.setObject(macAddress, forKey: "macAddress" as NSCopying)
            let webService = WebServices()
            webService.returnFailureResponseAlso = true
            webService.getUserLocationFromCMX(information) { (response) in
                if response != nil {
                    if let responseArray = response as? NSArray {
                        if responseArray.count > 0{
                            let info = responseArray[0] as? NSDictionary
                            if let mapCoordinate = info?.object(forKey: "mapCoordinate") as? NSDictionary {
                                let x = "\(mapCoordinate.object(forKey: "x")!)".toDouble()
                                let y = "\(mapCoordinate.object(forKey: "y")!)".toDouble()
                                let z = "\(mapCoordinate.object(forKey: "z")!)".toDouble()
                                var floorRefId = ""
                                if let mapInfo = info?.object(forKey: "mapInfo") as? NSDictionary {
                                    floorRefId = (mapInfo.object(forKey: "floorRefId") as! String)
                                }
                                var serverTimeStamp = ""
                                if let statistics = info?.object(forKey: "statistics") as? NSDictionary {
                                    serverTimeStamp = (statistics.object(forKey: "currentServerTime") as! String)
                                }
                                let currentLocation = VSCMXLocationUpdate(x: x!, y: y!, z: z!, floorId: floorRefId, deviceMACAddress: TEST_DEVICE_MAC_ADDRESS, serverTimeStamp: serverTimeStamp, capturedAt: NSDate() as Date)
                                self.completion!(currentLocation)
                            }else{
                                self.completion!(nil)
                            }
                        }else{
                            self.completion!(nil)
                        }
                    }else{
                        self.completion!(nil)
                    }
                }else{
                    self.completion!(nil)
                }
                self.reScheduleUpdate()
            }
        }
    }
    
}
