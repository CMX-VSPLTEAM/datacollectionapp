//
//  VSMotionController.swift
//  VSensor
//
//  Created by Virinchi Softwares on 14/09/16.
//  Copyright © 2016 Virinchi Softwares. All rights reserved.
//

import Foundation
import CoreMotion


/// Call back handlers , block based
typealias VSMCMFCompletionBlock = (_ magneticField:VSMagneticField) ->()
typealias VSMCACCompletionBlock = (_ acceleration:VSAccelerate) ->()
typealias VSMCRCompletionBlock = (_ rotation:VSRotation) ->()
typealias VSMCATCompletionBlock = (_ attitude:VSAttitude) ->()
typealias VSMCALCompletionBlock = (_ altitude:CMAltitudeData) ->()
typealias VSMCPDCompletionBlock = (_ pedometer:CMPedometerData) ->()
typealias VSMCMACompletionBlock = (_ motionActivity:CMMotionActivity) ->()
typealias VSMCMASCompletionBlock = (_ motionActivities:[CMMotionActivity]?) ->()


/// Manager class dealling with every type of motion events
class VSMotionController: NSObject {
    
    /// initialize instance
    static let sharedInstance : VSMotionController = {
        let instance = VSMotionController()
        return instance
    }()
    
    fileprivate var motionManager: CMMotionManager!
    fileprivate var pedometer: CMPedometer!
    fileprivate var altimeter: CMAltimeter!
    fileprivate var motionActivityManager: CMMotionActivityManager!
    
    fileprivate override init() {
        
    }
    
    /// to update motion update frequency
    func updateMotionUpdateTimeInterval() {
        motionManager.deviceMotionUpdateInterval = MOTION_UPDATE_INTERVAL
        motionManager.magnetometerUpdateInterval = MOTION_UPDATE_INTERVAL
    }
    
    func stopMotionUpdate () {
        motionManager.stopGyroUpdates()
        motionManager.stopMagnetometerUpdates()
        motionManager.stopDeviceMotionUpdates()
        motionManager = nil;
    }
    
    func isMotionManagerRunning () -> Bool {
        return motionManager != nil ? true : false;
    }
    
    /**
     call this method to get live motion updates
     
     - parameter completionMF: Magenetic field update callback
     - parameter completionAC: Accelerometer update callback
     - parameter completionR:  Gyroscope update callback
     - parameter completionAT: Attitude update callback
     */
    func startMotionUpdates(_ completionMF:@escaping VSMCMFCompletionBlock,_ completionAC:@escaping VSMCACCompletionBlock,_ completionR:@escaping VSMCRCompletionBlock,_ completionAT:@escaping VSMCATCompletionBlock) {
        
        if motionManager == nil {
            motionManager = CMMotionManager()
        }
        
        //set motion interval
        motionManager.deviceMotionUpdateInterval = MOTION_UPDATE_INTERVAL
        motionManager.magnetometerUpdateInterval = MOTION_UPDATE_INTERVAL

        if motionManager.isDeviceMotionAvailable {
            motionManager.startDeviceMotionUpdates(to: OperationQueue.main) { (deviceMotion, error) in
                if error != nil {
                    print("ERROR DEVICE MOTION UPDATES , \n ERROR DESCRIPTION : \(error!.localizedDescription)")
                }else if  deviceMotion != nil {
                    let capturedAt = NSDate()
                    
                    let ax = deviceMotion?.userAcceleration.x
                    let ay = deviceMotion?.userAcceleration.y
                    let az = deviceMotion?.userAcceleration.z
                    let accelerate = VSAccelerate(x: ax!, y: ay!, z: az!, capturedAt: capturedAt as Date)
                    
                    let rx = deviceMotion?.rotationRate.x
                    let ry = deviceMotion?.rotationRate.y
                    let rz = deviceMotion?.rotationRate.z
                    let rotation = VSRotation(x: rx!, y: ry!, z: rz!, capturedAt: capturedAt as Date, rotationRate: deviceMotion?.rotationRate)
                    
                    let yaw = deviceMotion?.attitude.yaw
                    let roll = deviceMotion?.attitude.roll
                    let pitch = deviceMotion?.attitude.pitch
                    let attitude = VSAttitude(yaw: yaw!, roll: roll!, pitch: pitch!, capturedAt: capturedAt as Date)
                    
                    
                    DispatchQueue.main.asyncAfter(deadline:.now()) {
                        completionAC(accelerate)
                        completionR(rotation)
                        completionAT(attitude)
                    }
                }
            }
        }else{
            showNotification(message: "Device Motion not available !")
        }
        if motionManager.isMagnetometerAvailable {
            //start magneto meter readings.. Since Device motion update din't return megntic field data
            motionManager.startMagnetometerUpdates(to: OperationQueue.main) {(magneticField, error) in
                let capturedAt = NSDate()
                let mfx = magneticField?.magneticField.x
                let mfy = magneticField?.magneticField.y
                let mfz = magneticField?.magneticField.z
                let magneticField = VSMagneticField(x: mfx!, y: mfy!, z: mfz!, capturedAt: capturedAt as Date)
                DispatchQueue.main.asyncAfter(deadline:.now()) {
                    completionMF(magneticField)
                }
            }
        }else{
            showNotification(message: "Magnetometer not available !")
        }
    }
    
    
    /**
     Start Activity update to main queue
     
     - parameter completionAL: Altitude value update callback
     - parameter completionPM: Pedometer value update callback
     - parameter completionMA: Motion Activity update callback
     */
    func startUpdatingActivities(_ completionAL:@escaping VSMCALCompletionBlock,_ completionPM:@escaping VSMCPDCompletionBlock,_ completionMA:@escaping VSMCMACompletionBlock) {
        
        pedometer = CMPedometer()
        altimeter = CMAltimeter()
        motionActivityManager = CMMotionActivityManager()
        if CMAltimeter.isRelativeAltitudeAvailable(){
            altimeter.startRelativeAltitudeUpdates(to: OperationQueue.main) {
                (altitudeData, error) in
                if error != nil {
                    print("ERROR ALTIMETER UPDATES , \n ERROR DESCRIPTION : \(error!.localizedDescription)")
                } else {
                    DispatchQueue.main.asyncAfter(deadline:.now()) {
                        completionAL(altitudeData!)
                    }
                }
            }
        }else{
            showNotification(message: "Altimeter not available !")
        }
        
        func startPedimeter(){
            pedometer.startUpdates(from: NSDate() as Date) {
                (pedometerData, error) in
                if error != nil {
                    print("ERROR PEDOMETER UPDATES , \n ERROR DESCRIPTION : \(error!.localizedDescription)")
                } else {
                    DispatchQueue.main.asyncAfter(deadline:.now()) {
                        completionPM(pedometerData!)
                    }
                }
            }
        }
        
  
        if CMPedometer.isStepCountingAvailable() {
            startPedimeter()
        }else {
            showNotification(message: "Pedometer not available !")
        }
//        if #available(iOS 10.0, *) {
//            if CMPedometer.isPedometerEventTrackingAvailable() {
//                startPedimeter()
//            }else{
//                showNotification(message: "Pedometer not available !")
//            }
//        } else {
//            startPedimeter()
//        }
//        

        
        if CMMotionActivityManager.isActivityAvailable() {
            motionActivityManager.startActivityUpdates(to: OperationQueue.main) {
                activityData in
                DispatchQueue.main.asyncAfter(deadline:.now()) {
                    completionMA(activityData!)
                }
            }
        }else{
            showNotification(message: "Activity not available !")
        }
    }
    
    
    /**
     Fetch history from MotionActivity
     
     - parameter completionMAS: MotionActivities update callback
     */
    func fetchHistoricalMotionActivities(_ completionMAS:@escaping VSMCMASCompletionBlock) {
        let oneWeekTimeInterval = 24 * 3600 as TimeInterval
        let fromDate = NSDate(timeIntervalSinceNow: -oneWeekTimeInterval)
        let toDate = NSDate()
        motionActivityManager.queryActivityStarting(from: fromDate as Date,to: toDate as Date, to: OperationQueue.main) {
            (activities, error) in
            if error != nil {
                print("ERROR QUERYING ACTIVITIES , \n ERROR DESCRIPTION : \(error!.localizedDescription)")
            }else{
                DispatchQueue.main.asyncAfter(deadline:.now()) {
                    completionMAS(activities?.reversed())
                }
            }
        }
    }
    
    
    /**
     Fetch pedometer data between provide data ranges
     
     - parameter from:         Start date
     - parameter to:           End data
     - parameter completionPD: Pedomoeter data update callback
     */
    func fetchPedometerData(from:NSDate,to:NSDate,_ completionPD:@escaping VSMCPDCompletionBlock) {
        pedometer.queryPedometerData(from: from as Date, to:to as Date) {
            (pedometerData, error) -> Void in
            if error != nil {
                print("ERROR QUERYING PEDOMETER DETAILS , \n ERROR DESCRIPTION : \(error!.localizedDescription)")
            } else {
                DispatchQueue.main.asyncAfter(deadline:.now()) {
                    completionPD(pedometerData!)
                }
            }
        }
    }
    
}
