//
//  VSCSVExporter.swift
//  VSensor
//
//  Created by Virinchi Softwares on 14/09/16.
//  Copyright © 2016 Virinchi Softwares. All rights reserved.
//

import UIKit
import Foundation

class VSCSVExporter: NSObject {
    
    /// <#Description#>
    ///
    /// - parameter fileName:      <#fileName description#>
    /// - parameter keyArray:      <#keyArray description#>
    /// - parameter variableArray: <#variableArray description#>
    ///
    /// - returns: <#return value description#>
    class func exportToCsv(_ fileName : NSString, keyArray:NSArray, variableArray:NSArray)->String{
        let content: NSString =  "\(keyArray.componentsJoined(by: ",") as NSString)\n" as NSString
        let stringObject : NSMutableString? = NSMutableString()
        let paths: NSArray = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray;
        let documentsDirectory : NSString = paths[0] as! NSString
        let filePathToWrite = "\(documentsDirectory)/\(fileName)"
        stringObject?.append("\(content)")
        let b : Int = (variableArray[0] as AnyObject).count as Int
        for j in 0..<b {
            let stringValue : NSMutableString = NSMutableString()
            let varArrayCount : Int = variableArray.count as Int
            for k in 0..<varArrayCount{
                let newVar = variableArray[k] as! NSArray
                stringValue.append("\(newVar[j]),")
            }
            let original : NSString  = "\(stringValue)" as NSString
            let locationInt : NSRange = original.range(of: ",", options: NSString.CompareOptions.backwards)
            var newStr : NSString = ""
            if(locationInt.location != NSNotFound){
                newStr = "\(stringValue)" as NSString
                newStr = newStr.replacingCharacters(in: locationInt, with: "\n") as NSString
            }
            stringObject?.append(newStr as String)
        }
        do{
            try stringObject?.write(toFile: filePathToWrite, atomically: false, encoding: String.Encoding.utf8.rawValue)
        }catch{
            print(error)
        }
        return filePathToWrite
    }
}
