//
//  WebServices.swift
//  Application
//
//  Created by Virinchi Softwares on 28/09/16.
//  Copyright © 2016 Virinchi Softwares. All rights reserved.
//

//MARK: - WebServices : This class handles communication of application with its Server.

import Foundation
import AFNetworking

//LIVE
var BASE_URL = "http://128.107.3.20/"

//MARK: - Url's
struct ServerSupportURLS {
    static let GET_USER_LIVE_LOCATION = "api/location/v2/clients"
}

//MARK: - Response Error Handling Options
enum ResponseErrorOption {
    case dontShowErrorResponseMessage
    case showErrorResponseWithUsingNotification
}

//MARK: - Completion block
typealias WSCompletionBlock = (_ responseData :AnyObject?) ->()
typealias WSCompletionBlockForFile = (_ responseData :NSData?) ->()

//MARK: - Custom methods
extension WebServices {
    //MARK: - Authentication
    
    func getUserLocationFromCMX(_ information: NSDictionary ,completionBlock: @escaping WSCompletionBlock) -> () {
        let parameters = NSMutableDictionary()
        copyData(information, sourceKey: "macAddress", destinationDictionary: parameters, destinationKey: "macAddress", methodName:#function)
        addCommonInformation(parameters)
        performGetRequest(parameters, urlString: String(BASE_URL+ServerSupportURLS.GET_USER_LIVE_LOCATION) as NSString, completionBlock: completionBlock,methodName:#function)
    }
}

//MARK: - Private
class WebServices: NSObject {
    
    var completionBlock: WSCompletionBlock?
    var responseErrorOption: ResponseErrorOption?
    var progressIndicatorText: String?
    var returnFailureResponseAlso = false
    
    /// Check for cache and return from cache if possible.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: bool (wether cache was used or not)
    func loadFromCacheIfPossible(_ body:NSDictionary? , urlString:NSString? ,completionBlock: WSCompletionBlock , maxAgeInSeconds:Float)->(Bool){
        return false
    }
    
    func updateSecurityPolicy(_ manager:AFHTTPSessionManager){
        let securityPolicy = AFSecurityPolicy(pinningMode: AFSSLPinningMode.none)
        securityPolicy.validatesDomainName = false
        securityPolicy.allowInvalidCertificates = true
        manager.securityPolicy = securityPolicy
    }
    
    func addAuthenticationInformationInHeader(_ manager:AFHTTPSessionManager) {
        manager.requestSerializer.setAuthorizationHeaderFieldWithUsername(CMX_ACCESS_USER_NAME, password: CMX_ACCESS_USER_NAME)
    }
    
    /// Perform  GET Request.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: parsed server response via completionBlock
    func performGetRequest(_ body:NSDictionary? , urlString:AnyObject? ,completionBlock: @escaping WSCompletionBlock,methodName:String)->(){
        DispatchQueue.global().async {
            if isInternetConnectivityAvailable(true)==false {
                return;
            }
            let url = URL(string: urlString as! String)
            
            print("\n\n\n                  HITTING URL\n\n \(url!.absoluteString)\n\n\n                  WITH GET REQUEST\n\n\(body ?? nil)\n\n" )
            if self.progressIndicatorText != nil{
                showActivityIndicator(self.progressIndicatorText! as NSString);
            }
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.requestSerializer.timeoutInterval = REQUEST_TIME_OUT
            self.updateSecurityPolicy(manager)
            self.addCommonInformationInHeader(manager.requestSerializer)
            self.addAuthenticationInformationInHeader(manager)
            manager.get(urlString as! String, parameters: body, progress: { (progress) in
                if self.progressIndicatorText != nil{
                    progressForShowingOnActivityIndicator = progress
                }
                }, success: { (urlSessionDataTask, responseObject) in
                    self.verifyServerResponse(responseObject as AnyObject, error: nil, completionBlock: completionBlock,methodName: methodName as NSString?)
                }, failure: { (urlSessionDataTask, error) in
                    self.verifyServerResponse(nil, error: error as NSError, completionBlock: completionBlock,methodName: methodName as NSString?)
            })
        }
    }
    
    /// Perform Get Request For Downloading file data.
    ///
    /// - parameter url: url to download file
    /// - returns: fileData via completionBlock
    func performDownloadGetRequest(_ urlString:NSString? ,completionBlock: @escaping WSCompletionBlockForFile,methodName:String)->(){
        DispatchQueue.global().async {
            if isInternetConnectivityAvailable(true)==false {
                return;
            }
            if self.progressIndicatorText != nil{
                showActivityIndicator(self.progressIndicatorText! as NSString);
            }
            let url = URL(string: urlString as! String)
            print("\n\n\n                  HITTING URL TO DOWNLOAD FILE\n\n \((url!.absoluteString))\n\n\n" )
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.requestSerializer = AFHTTPRequestSerializer()
            manager.requestSerializer.timeoutInterval = REQUEST_TIME_OUT
            self.updateSecurityPolicy(manager)
            self.addCommonInformationInHeader(manager.requestSerializer)
            self.addAuthenticationInformationInHeader(manager)
            manager.get(urlString as! String, parameters: nil, progress: { (progress) -> Void in
                if self.progressIndicatorText != nil{
                    progressForShowingOnActivityIndicator = progress
                }
                }, success: { (urlSessionDataTask, responseObject) -> Void in
                    completionBlock(responseObject as! NSData?)
                    DispatchQueue.main.async {
                        if self.progressIndicatorText != nil{
                            hideActivityIndicator()
                        }
                    }
            }){(urlSessionDataTask, error) -> Void in
                self.showServerNotRespondingMessage()
                DispatchQueue.main.async {
                    if self.progressIndicatorText != nil{
                        hideActivityIndicator()
                    }
                }
            }
        }
    }
    
    /// Perform Json Encoded Post Request.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: parsed server response via completionBlock
    func performJsonPostRequest(_ body:NSDictionary? , urlString:NSString ,completionBlock: @escaping WSCompletionBlock,methodName:String)->(){
        DispatchQueue.global().async {
            if isInternetConnectivityAvailable(true)==false {
                return;
            }
            if self.progressIndicatorText != nil{
                showActivityIndicator(self.progressIndicatorText! as NSString);
            }
            let url = URL(string: urlString as String)
            print("\n\n\n                  HITTING URL\n\n \((url!.absoluteString))\n\n\n                  WITH POST JSON BODY\n\n\(body ?? nil)\n\n" )
            
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.requestSerializer = AFJSONRequestSerializer()
            manager.requestSerializer.timeoutInterval = REQUEST_TIME_OUT
            self.updateSecurityPolicy(manager)
            self.addCommonInformationInHeader(manager.requestSerializer)
            self.addAuthenticationInformationInHeader(manager)
            manager.post(urlString as String, parameters: body, progress: { (progress) in
                if self.progressIndicatorText != nil{
                    progressForShowingOnActivityIndicator = progress
                }
                }, success: { (urlSessionDataTask, responseObject) in
                    self.verifyServerResponse(responseObject as AnyObject, error: nil, completionBlock: completionBlock,methodName: methodName as NSString?)
                }, failure: { (urlSessionDataTask, error) in
                    self.verifyServerResponse(nil, error: error as NSError, completionBlock: completionBlock,methodName: methodName as NSString?)
            })
        }
    }
    /// Perform Post Request.
    ///
    /// - parameter body: parameters to set in Body of the request
    /// - returns: parsed server response via completionBlock
    func performPostRequest(_ body:NSDictionary? , urlString:NSString? ,completionBlock: @escaping WSCompletionBlock ,methodName:NSString?)->(){
        DispatchQueue.global().async {
            if isInternetConnectivityAvailable(true)==false {
                return;
            }
            if self.progressIndicatorText != nil{
                showActivityIndicator(self.progressIndicatorText! as NSString);
            }
            let url = URL(string: urlString as! String)
            print("\n\n\n                  HITTING URL\n\n \((url!.absoluteString))\n\n\n                  WITH POST BODY\n\n\(body ?? nil)\n\n" )
            let manager = AFHTTPSessionManager()
            manager.responseSerializer = AFHTTPResponseSerializer()
            manager.requestSerializer = AFHTTPRequestSerializer()
            manager.requestSerializer.timeoutInterval = REQUEST_TIME_OUT
            self.updateSecurityPolicy(manager)
            self.addCommonInformationInHeader(manager.requestSerializer)
            self.addAuthenticationInformationInHeader(manager)
            manager.post(urlString as! String, parameters: body, progress: { (progress) in
                if self.progressIndicatorText != nil{
                    progressForShowingOnActivityIndicator = progress
                }
                }, success: { (urlSessionDataTask, responseObject) in
                    self.verifyServerResponse(responseObject as AnyObject, error: nil, completionBlock: completionBlock,methodName: methodName as NSString?)
                }, failure: { (urlSessionDataTask, error) in
                    self.verifyServerResponse(nil, error: error as NSError, completionBlock: completionBlock,methodName: methodName as NSString?)
            })
        }
    }
    
    /// Add commonly used parameters to all request.
    ///
    /// - parameter information: pass the dictionary object here that you created to hold parameters required. This function will add commonly used parameter into it.
    func addCommonInformation(_ information:NSMutableDictionary?)->(){
    }
    
    func addCommonInformationInHeader(_ requestSerialiser:AFHTTPRequestSerializer?)->(){
    }
    
    /// To display server not responding message via notification banner.
    func showServerNotRespondingMessage(){
        DispatchQueue.main.async {
            let showMessage = true
            if showMessage {
                showNotification(message: MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY , onlyForDebugging: false)
            }
        }
    }
    
    /// To check wether the server operation succeeded or not.
    /// - returns: bool
    func isSuccess(_ response:AnyObject?)->(Bool){
        if response != nil{
            return true
        }
        return false
    }
    
    /// To verify the server response received and perform action on basis of that.
    /// - parameter response: data received from server in the form of NSData
    func verifyServerResponse(_ response:AnyObject?,error:NSError?,completionBlock: WSCompletionBlock?,methodName:NSString?)->(){
        if responseErrorOption == nil {
            responseErrorOption = ResponseErrorOption.showErrorResponseWithUsingNotification
        }
        if progressIndicatorText != nil{
            hideActivityIndicator();
        }
        if error != nil {
            if responseErrorOption != ResponseErrorOption.dontShowErrorResponseMessage {
                if error?.localizedDescription != nil{
                    showNotification(error!.localizedDescription, showOnNavigation: false, showAsError: true)
                }else{
                    showServerNotRespondingMessage()
                }
            }
            printErrorMessage(error, methodName: #function)
            DispatchQueue.main.async(execute: {
                completionBlock!(nil)
            })
        }
        else if response != nil {
            DispatchQueue.global().async {
                let responseObject = parsedJsonFrom(response as? Data,methodName: methodName!)
                if isNotNull(responseObject) {
                    if self.isSuccess(responseObject){
                        DispatchQueue.main.async(execute: {
                            completionBlock!(responseObject)
                        })
                    }
                    else {
                        if self.responseErrorOption == ResponseErrorOption.showErrorResponseWithUsingNotification {
                            showNotification(MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY, showOnNavigation: false, showAsError: true)
                        }
                        if self.returnFailureResponseAlso {
                            DispatchQueue.main.async(execute: {
                                completionBlock!(responseObject)
                            })
                        }else{
                            DispatchQueue.main.async(execute: {
                                completionBlock!(nil)
                            })
                        }
                    }
                }
                else {
                    if self.responseErrorOption == ResponseErrorOption.showErrorResponseWithUsingNotification {
                        showNotification(MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY, showOnNavigation: false, showAsError: true)
                    }
                    DispatchQueue.main.async(execute: {
                        completionBlock!(nil)
                    })
                }
            }
        }
        else {
            if responseErrorOption == ResponseErrorOption.showErrorResponseWithUsingNotification {
                showNotification(MESSAGE_TEXT___FOR_SERVER_NOT_REACHABILITY, showOnNavigation: false, showAsError: true)
            }
            DispatchQueue.main.async(execute: {
                completionBlock!(nil)
            })
        }
    }
}

